from unittest.mock import patch

import pytest

from oidc_client.config import ProviderConfig
from oidc_client.discovery import fetch_provider_config
from oidc_client.error import ProviderConfigError

from .utils import asio


def test_fetch_provider_config():
    test_issuer = "https://gitlab.com"
    provier_config = fetch_provider_config(test_issuer)

    assert isinstance(provier_config, ProviderConfig)
    assert provier_config.issuer == test_issuer
    assert provier_config.authorization_endpoint.startswith("https://")
    assert provier_config.token_endpoint.startswith("https://")


@pytest.mark.parametrize("issuer", ["http://example.com", "example.com"])
def test_bad_provider_config(issuer):
    with pytest.raises(ValueError) as excinfo:
        fetch_provider_config(issuer)
    assert "must be HTTPS" in str(excinfo.value)


def test_fetch_provider_response_issuer_mismatch():
    issuer_query = "https://example.com"
    issuer_response = "https://bad.example.com"
    assert issuer_query != issuer_response

    with (
        patch(
            "oidc_client.discovery.urlopen",
            return_value=asio(
                ProviderConfig(
                    issuer_response,
                    authorization_endpoint="https://bad.example.com/oauth2/authorize",
                    token_endpoint="https://bad.example.com/oauth2/token",
                )
            ),
        ),
        pytest.raises(ProviderConfigError),
    ):
        fetch_provider_config(issuer_query)
