import json
from subprocess import PIPE, Popen


def test__get_provider_meta():
    with Popen(
        ["oidc", "get-provider-metadata", "https://gitlab.com"],
        stdout=PIPE,
        stderr=PIPE,
    ) as proc:
        assert proc.stderr.read() == b""

        data = json.load(proc.stdout)
        assert data["issuer"] == "https://gitlab.com"
        assert "authorization_endpoint" in data
        assert "token_endpoint" in data
